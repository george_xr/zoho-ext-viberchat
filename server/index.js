/*
Copyright (c) 2017, ZOHO CORPORATION
License: MIT
*/

var express = require('express');
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var morgan = require('morgan');
var serveIndex = require('serve-index');
var http = require('http');
var https = require('https');
var chalk = require('chalk');
const request = require('request')
const Emitter = require("events");
let emitter = new Emitter();
//const chats = require('./chat');
var URL = 'https://damp-tundra-61257.herokuapp.com'

process.env.PWD = process.env.PWD || process.cwd(); 


var expressApp = express();
var port = 5000; 

expressApp.set('port', port);
expressApp.use(morgan('dev'));
expressApp.use(bodyParser.json());
expressApp.use(bodyParser.urlencoded({extended: false}));
expressApp.use(errorHandler());


expressApp.use('/', function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();
});

expressApp.get('/plugin-manifest.json', function(req, res) {
  res.sendfile('plugin-manifest.json');
});

expressApp.use('/app', express.static('app'));
expressApp.use('/app', serveIndex('app')); 


expressApp.post('/chat', function(req, res){
	chat(req.body.msg);
});

expressApp.post('/zohoSaveRead', function(req, res){
	let name = req.body;
	let url = URL + '/zohoSaveRead';
	request.post(url, {form: {msg: name}}, function(err, body, response) {
		if(err) {
			console.log('--------- error request /zohoSaveRead------ ' + err);
		}
	})
});			

expressApp.get('/getchat', function(req, res){	
	let url = URL + '/getchat';
	request.get(url, function(err, body, response) {
		if(err) {
			console.log('--------- error request getChat------ ' + err);
		}
	})
});				      

expressApp.post('/', function(req, res) {
  res.redirect('/app');
});


var server = https.createServer(expressApp).listen(port, function(){
  console.log('Zet running at ht'+'tp://127.0.0.1:' + port);
}).on('error', function(err) {
  if(err.code === 'EADDRINUSE') {
    console.log(chalk.bold.red(port + " port is already in use"));
  }
});
var SocketServer = new require('ws');
var clients = {};

var webSocketServer = new SocketServer.Server({
	server//: server,
});

webSocketServer.on('connection', function(ws) {
	var id = Math.random();
	clients[id] = ws;
	console.log("новое соединение " + id);
	for(var key in clients){
		console.log(key);
		
	}
	ws.on('message', function(message) {
	  console.log('получено сообщение ' + message);

	  let url = URL + '/zoho';
	  request.post(url, {form: {message: message}}, function(err, body, res) {
			if(err) {
				console.log('--------- error request ------ ' + err);
			}
		})
//Support msg
		for (var key in clients) {
			clients[key].send(message);
		}
	});

	//User msg
	emitter.on('msgFromViber', function(msg){
		for (var key in clients) {
			clients[key].send(msg);
		}
	})

	ws.on('close', function() {
		console.log('соединение закрыто ' + id);
		emitter.removeAllListeners('msgFromViber')
		delete clients[id];
	});
});

function chat(message){
	emitter.emit('msgFromViber', message);
}

emitter.on('error', function(err){
	console.log(err);
})